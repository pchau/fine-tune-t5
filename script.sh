#!/bin/bash
 
# SLURM OPTIONS
#SBATCH --partition=gpu-a40   # Partition is a queue for jobs
#SBATCH --time=3-00:00:00         # Time limit for the job
#SBATCH --job-name=Pretrain     # Name of your job
#SBATCH --error=job-%j.err
#SBATCH --output=job-%j.out
#SBATCH --nodes=1               # Number of nodes you want to run your process on
#SBATCH --nodelist=l3icalcul05 #The name of the node to use
#SBATCH --ntasks-per-node=32  # Number of CPU cores 
#SBATCH --mem=40GB
#SBATCH --gres=gpu:6      # Number of GPUs

#df -h
export NCCL_DEBUG=INFO
#nvidia-smi 
accelerate launch --multi_gpu --num_processes 6 --mixed_precision='no' bart.py --with_tracking 
#python test.py
