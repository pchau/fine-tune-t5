import os
import numpy as np
import json
from tqdm.auto import tqdm
import nltk

import datasets
from datasets import load_dataset
import evaluate

import torch
from torch.utils.data import DataLoader 
import transformers
from transformers import (
    AutoTokenizer,
    AutoModelForSeq2SeqLM,
    DataCollatorForSeq2Seq
)

from accelerate import Accelerator
accelerator = Accelerator()

max_source_length = 256
max_target_length = 128
batch_size = 64

##### GET THE DATASET
# In distributed training, the load_dataset func guarantee that only one local process can concurrently download the dataset 
raw_datasets = load_dataset('ccdv/pubmed-summarization')
# filter out len(article)=0
raw_datasets = raw_datasets.filter(lambda x: len(x['article'].split()) > 0)
print(raw_datasets)

##### Get pretrained model and tokenizer 
# in distributed training, .from_pretrained methods guarantee that only 1 local process can concurrently down model & vocab
tokenizer = AutoTokenizer.from_pretrained('t5base_256-128', local_files_only=True)
# download the pretrained model and fine-tune
model = AutoModelForSeq2SeqLM.from_pretrained('t5base_256-128',  local_files_only=True)

##### PREPROCESSING
column_names = raw_datasets["train"].column_names

# add prefix for input 
prefix = 'summarize: '
def preprocess_function(examples):
    # add prefix to the inputs
    inputs = [prefix + doc for doc in examples['article']]
    # tokenizer documents
    model_inputs = tokenizer(inputs, max_length=max_source_length, truncation=True)

    # tokenizer for targets
    labels = tokenizer(text_target = examples['abstract'], max_length=max_target_length, truncation=True)

    model_inputs['labels'] = labels['input_ids']
    return model_inputs

# apply func on all pairs of sentences
# executed only by the main process
test_dataset = raw_datasets["test"].map(
    preprocess_function,
    batched=True,
    remove_columns=column_names,
)
print(test_dataset)

# Data Collator: pad the inputs/labels to the maximum length in the batch 
# return tensors: pytorch
data_collator = DataCollatorForSeq2Seq(tokenizer, model=model, return_tensors="pt")

##### Define training/testing sets  to train model 
# columns: which columns serving as independent variables
# batch_size: for training 
# shuffle: whether want to shuffle dataset
# collate_fn: collator function 

# integrate datasets with collator 
test_dataloader = DataLoader(test_dataset, collate_fn=data_collator, batch_size=batch_size)

##### BUILDING & COMPLILING THE MODEL
# feed our model, optimizer, and dataloaders to the accelerator 
model, test_dataloader = accelerator.prepare(
    model, test_dataloader
)

# ROUGE metric expect: generated summaries into sentences that are separated by newlines
def postprocess_text(preds, labels):
    preds = [pred.strip() for pred in preds]
    labels = [label.strip() for label in labels]

    # ROUGE expects a newline after each sentence
    preds = ["\n".join(nltk.sent_tokenize(pred)) for pred in preds]
    labels = ["\n".join(nltk.sent_tokenize(label)) for label in labels]

    return preds, labels
    
# Load metric
rouge_score = evaluate.load("rouge")

##### EVALUATING
total_batch_size = batch_size * accelerator.num_processes

print("***** Running testing *****")
print(f"  Num examples = {len(test_dataset)}")
print(f"  Batch size = {batch_size}")
print(f"  Total test batch size = {total_batch_size}")

# Only show the progress bar once on each machine.
progress_bar = tqdm(range(len(test_dataloader)))    
# Evaluation
model.eval()
for step, batch in enumerate(test_dataloader):
    with torch.no_grad():
        # generate summaries for each epoch 
        # gen token
        generated_tokens = accelerator.unwrap_model(model).generate(
            batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length = max_target_length
        )

        generated_tokens = accelerator.pad_across_processes(
            generated_tokens, dim=1, pad_index=tokenizer.pad_token_id
        )
        labels = batch["labels"]

        # If we did not pad to max length, we need to pad the labels too
        labels = accelerator.pad_across_processes(
            batch["labels"], dim=1, pad_index=tokenizer.pad_token_id
        )

        generated_tokens = accelerator.gather(generated_tokens).cpu().numpy()
        labels = accelerator.gather(labels).cpu().numpy()

        # Replace -100 in the labels as we can't decode them
        labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
        if isinstance(generated_tokens, tuple):
            generated_tokens = generated_tokens[0]
        # decode them into text 
        decoded_preds = tokenizer.batch_decode(
            generated_tokens, skip_special_tokens=True
        )
        decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)

        decoded_preds, decoded_labels = postprocess_text(
            decoded_preds, decoded_labels
        )

        rouge_score.add_batch(predictions=decoded_preds, references=decoded_labels)
        progress_bar.update(1)

# Extract the median ROUGE scores
result = rouge_score.compute()
result = {k: round(v * 100, 4) for k, v in result.items()}

# Save and upload
accelerator.wait_for_everyone()
if accelerator.is_main_process:
    print(result)
    with open("test/t5_base.json", "w") as f:
        json.dump(result, f)




